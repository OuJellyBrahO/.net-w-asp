﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Lab06.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class DepartmentController : Controller
    {
        private IDepartmentRepository deptRepo;
        private int PageSize = 3;

        public DepartmentController(IDepartmentRepository repo)
        {
            deptRepo = repo;
        }

        public ViewResult List(int pageNo = 1)
        {
            DepartmentListViewModel model = new DepartmentListViewModel();
            IEnumerable<Department> pagedDepartments;

            pagedDepartments = deptRepo.Departments
                .OrderBy(d => d.DepartmentName)
                .Skip((pageNo - 1) * PageSize)
                .Take(PageSize);

            model.Departments = pagedDepartments;

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = deptRepo.Departments.Count()

            };

            model.PagingInfo = pInfo;
            return View(model);

        }
        

        
    }
}