﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lab06
{
    public class Startup
    {
        IConfigurationRoot Configuration;
        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json").Build();
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            
            services.AddDbContext<StoreDbContext>(options =>
                options.UseSqlServer(
                    Configuration["Data:AcmeDepartmentEmployees:ConnectionString"]));

            services.AddTransient<IEmployeeRepository, EFEmployeeRepository>();
            services.AddTransient<IDepartmentRepository, EFDepartmentRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            

            app.UseDefaultFiles();
            app.UseMvcWithDefaultRoute();
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();

            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                        name: null,
                        template: "",
                        defaults: new { controller = "Department", action = "List"}
                        );

                    routes.MapRoute(
                        name: null,
                        template: "Page{pageNo:int}",
                        defaults: new { controller = "Department", action = "List", pageNo = 1}
                        );
                }
                );
        }
    }
}
