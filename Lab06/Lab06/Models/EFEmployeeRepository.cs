﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFEmployeeRepository : IEmployeeRepository
    {
        private StoreDbContext context;
        public IEnumerable<Employee> Employees => context.Employees
            .Include(t => t.Title)
            ;

        public EFEmployeeRepository(StoreDbContext context)
        {
            this.context = context;
        }
    }
}
