﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lab06.Models
{
    public class Employee
    {
        //create the feilds for this table
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime HireDate { get; set; }
        public DateTime? TerminationDate { get; set; }


        //Associations
        public Title Title { get; set; }
        public int TitleId { get; set; }
        

        public List<DepartmentEmployee> DepartmentEmployees { get; set; }
        public List<DepartmentManager> DepartmentManagers { get; set; }


    }

    
}