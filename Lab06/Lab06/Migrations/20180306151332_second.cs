﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Lab06.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "hourlyWage",
                table: "Employees");

            migrationBuilder.AddColumn<decimal>(
                name: "hourlyWage",
                table: "Titles",
                type: "Money",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "hourlyWage",
                table: "Titles");

            migrationBuilder.AddColumn<decimal>(
                name: "hourlyWage",
                table: "Employees",
                type: "Money",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
