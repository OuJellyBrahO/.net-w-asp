﻿using PartyInvites.Models;
using Razor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Razor.Tests
{
    public class GuestResponseTest
    {



        [Fact]
        public void RepoStoresResponses()
        {
            //Arrange
            IEnumerable<GuestResponse> guestResponse = Repository.Responses;
            int numResponse = guestResponse.Count();
            

            //Act
            GuestResponse r = new GuestResponse { Name = "Bob", Email = "test@test.com", Phone = "6085555555", WillAttend = true };
            Repository.AddResponse(r);

            //Assert
            Assert.NotEqual(Repository.Responses.Count(), numResponse);
            Assert.Equal(Repository.Responses.Count(), (numResponse + 1));

        }


        [Fact]
        public void ChangeResponseBoolean()
        {
            //Arrange
            GuestResponse r = new GuestResponse { Name = "Bob", Email = "test@test.com", Phone = "6085555555", WillAttend = true };
            GuestResponse r2 = new GuestResponse { Name = "Sue", Email = "test@test.com", Phone = "6085555555", WillAttend = false };
            bool? rBoolExpected = false;
            bool? r2BoolExpected = true;

            //Act
            r.WillAttend = !r.WillAttend;
            r2.WillAttend = !r2.WillAttend;

            //Assert
            Assert.Equal(r.WillAttend, rBoolExpected);
            Assert.Equal(r2.WillAttend, r2BoolExpected);
        }
    }
}
