﻿INSERT INTO publishers (PublisherName, City, State, Country) VALUES ('Abatis Publishers','New York','NY','USA');
INSERT INTO publishers (PublisherName, City, State, Country) VALUES('Core Dump Books','San Francisco','CA','USA');
INSERT INTO publishers (PublisherName, City, State, Country) VALUES('Schadenfreude Press','Hamburg',NULL,'Germany');
INSERT INTO publishers (PublisherName, City, State, Country) VALUES('Tenterhooks Press','Berkeley','CA','USA');

INSERT INTO authors (FirstName, LastName, Phone, Address, City, State, Zip) VALUES('Sarah','Buchman','718-496-7223','75 West 205 St','Bronx','NY','10468');
INSERT INTO authors (FirstName, LastName, Phone, Address, City, State, Zip) VALUES('Wendy','Heydemark','303-986-7020','2922 Baseline Rd','Boulder','CO','80303');
INSERT INTO authors (FirstName, LastName, Phone, Address, City, State, Zip) VALUES('Hallie','Hull','415-549-4278','3800 Waldo Ave, #14F','San Francisco','CA','94123');
INSERT INTO authors (FirstName, LastName, Phone, Address, City, State, Zip) VALUES('Klee','Hull','415-549-4278','3800 Waldo Ave, #14F','San Francisco','CA','94123');
INSERT INTO authors (FirstName, LastName, Phone, Address, City, State, Zip) VALUES('Christian','Kells','212-771-4680','114 Horatio St','New York','NY','10014');
INSERT INTO authors (FirstName, LastName, Phone, Address, City, State, Zip) VALUES('','Kellsey','650-836-7128','390 Serra Mall','Palo Alto','CA','94305');
INSERT INTO authors (FirstName, LastName, Phone, Address, City, State, Zip) VALUES('Paddy','O''Furniture','941-925-0752','1442 Main St','Sarasota','FL','34236');


--delete from titles;
--DBCC CHECKIDENT ('Titles', RESEED, 0)
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('1977!','history',1,107,21.99,566,'2000-08-01',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('200 Years of German Humor','history',3,14,19.95,9566,'1998-04-01',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('Ask Your System Administrator','computer',2,1226,39.95,25667,'2000-09-01',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('But I Did It Unconsciously','psychology',4,510,12.99,13001,'1999-05-31',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('Exchange of Platitudes','psychology',4,201,6.95,201440,'2001-01-01',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('How About Never?','biography',1,473,19.95,11320,'2000-07-31',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('I Blame My Mother','biography',3,333,23.95,1500200,'1999-10-01',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('Just Wait Until After School','children',4,86,10.00,4095,'2001-06-01',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('Kiss My Boo-Boo','children',4,22,13.95,5000,'2002-05-31',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('Not Without My Faberge Egg','biography',1,NULL,NULL,NULL,NULL,0);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('Perhaps It''s a Glandular Problem','psychology',4,826,7.99,94123,'2000-11-30',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('Spontaneous, Not Annoying','biography',1,507,12.99,100001,'2000-08-31',1);
INSERT INTO titles (TitleName, Type, PublisherId, Pages, Price, Sales, PubDate, Contract) VALUES('What Are The Civilian Applications?','history',3,802,29.99,10467,'1999-05-31',1);


--delete from titleauthors;
--DBCC CHECKIDENT ('TitleAuthors', RESEED, 0)
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(1,1,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(2,1,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(3,5,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(4,3,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(4,4,2);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(5,4,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(6,2,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(7,2,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(7,4,2);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(8,6,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(9,6,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(10,2,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(11,3,2);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(11,4,3);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(11,6,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(12,2,1);
INSERT INTO titleauthors (TitleId, AuthorId, AuthorOrder) VALUES(13,1,1);