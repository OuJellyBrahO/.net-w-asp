﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksEFCore.Models
{
    public interface ITitleRepository
    {
        IEnumerable<Title> Titles { get;  }
    }
}
