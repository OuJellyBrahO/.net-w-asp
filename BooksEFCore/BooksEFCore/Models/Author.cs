﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksEFCore.Models
{
    public class Author
    {
        //Create the feilds for this table
        public int AuthorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        //Associations
        //Join it to another table, in this case the TitleAuthors table
        public List<TitleAuthor> TitleAuthors { get; set; }
    }
}
