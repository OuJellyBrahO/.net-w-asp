﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_03.Models;
using Microsoft.AspNetCore.Mvc;

namespace LAB_03.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            Product[] arr = new Product[]
                {
                    new Product{Name = "Xbox One", Price = 299M, Description = "Microsoft Gaming Console", Category = "Gaming" },
                    new Product{Name = "Playstation 4", Price = 299M, Description = "Sony Gaming Console", Category = "Gaming" },
                    new Product{Name = "Samsung S8", Price = 899M, Description = "Samsung Mobile Phone", Category = "Mobile" },
                    new Product{Name = "Iphone X", Price = 1099M, Description = "Apple Mobile Phone", Category = "Mobile" },
                    new Product{Name = "Bosch Dishwasher", Price = 499M, Description = "Bosch Diswasher", Category = "Appliances" },
                    new Product{Name = "LG Dishwasher", Price = 599M, Description = "LG Diswasher", Category = "Appliances" }

                };

            ViewBag.Department = "Mobile";
            ViewBag.Rebate = .1M;

            return View(arr);

        }
    }
}