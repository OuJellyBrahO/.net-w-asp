﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LanguageFeatures.Models;
using Microsoft.AspNetCore.Mvc;

namespace LanguageFeatures.Controllers
{
    public class HomeController : Controller
    {
        //bool FilterByPrice(Product p) {
        //    return (p?.Price ?? 0) >= 20;
        //}


        //public ViewResult Index()
        //{
        //    return View(new string[] { "C#", "Language", "Features" });
        //}

        //public ViewResult Index()
        //{
        //    List<string> results = new List<string>();
        //    foreach (Product p in Product.GetProducts())
        //    {
        //        string name = p?.Name ?? "<No Name>";
        //        decimal? price = p?.Price ?? 0;
        //        //results.Add(
        //        //    string.Format("Name: {0}, Price {1}, Related: {2}",
        //        //    name,
        //        //    price,
        //        //    p?.Related?.Name ?? "<No Relation>"));

        //        results.Add($"Name: {name}, Price: {price:C2}, Related: {p?.Related?.Name ?? "<No Relation>"}");
        //    }

        //    return View(results);
        //}

        //public async Task<ViewResult> Index()
        //{
        //    long? length = await AsyncMethods.GetPageLength();
        //    return View(new string[] { $"Length : {length}" });
        //}

        //public ViewResult Index() {
        //    string[] names = new string[3];
        //    names[0] = "Bob";
        //    names[1] = "Joe";
        //    names[2] = "Alice";
        //    return View("Index", names);
        //}

        //public ViewResult Index() => View("Index", new string[] { "Bob", "Joe", "Alice"});

        //public ViewResult Index() {
        //    Dictionary<string, Product> products = new Dictionary<string, Product> {
        //        { "Kayak", new Product { Name = "Kayak", Price = 275M } },
        //        { "LifeJacket", new Product { Name = "Lifejacket", Price = 48.95M } }
        //    };
        //    return View("Index", products.Keys);
        //}

        //public ViewResult Index() {
        //    Dictionary<string, Product> products = new Dictionary<string, Product>
        //    {
        //        ["Kayak"] = new Product { Name = "Kayak", Price = 275M },
        //        ["LifeJacket"] = new Product { Name = "Lifejacket", Price = 48.95M }
        //    };

        //    return View("Index", products.Keys);
        //}

        //public ViewResult Index() {
        //    ShoppingCart cart
        //        = new ShoppingCart { Products = Product.GetProducts() };
        //    decimal cartTotal = cart.TotalPrices();
        //    return View("Index", new string[] { $"Total: {cartTotal:C2}" });
        //}

        //public ViewResult Index() {
        //    ShoppingCart cart
        //        = new ShoppingCart { Products = Product.GetProducts() };

        //    Product[] productArray = {
        //        new Product { Name = "Kayak", Price = 275M},
        //        new Product { Name = "Lifejacket", Price = 48.95M }
        //    };

        //    decimal cartTotal = cart.TotalPrices();
        //    decimal arrayTotal = productArray.TotalPrices();

        //    return View("Index", new string[] { $"Total: {cartTotal:C2}" , $"Array Total: {arrayTotal:C2}"});
        //}

        //public ViewResult Index() {
        //    Product[] productArray = {
        //        new Product { Name = "Kayak", Price = 275M},
        //        new Product { Name = "Lifejacket", Price = 48.95M },
        //        new Product { Name = "Soccer Ball", Price = 19.50M },
        //        new Product { Name = "Corner Flag", Price = 34.95M }
        //    };

        //    decimal arrayTotal = productArray.FilterByPrice(20).TotalPrices();

        //    return View("Index", new string[] { $"Total: {arrayTotal:C2}" });
        //}

        //public ViewResult Index()
        //{
        //    Product[] productArray = {
        //        new Product { Name = "Kayak", Price = 275M},
        //        new Product { Name = "Lifejacket", Price = 48.95M },
        //        new Product { Name = "Soccer Ball", Price = 19.50M },
        //        new Product { Name = "Corner Flag", Price = 34.95M }
        //    };

        //    Func<Product, bool> nameFilter = delegate (Product prod) {
        //        return prod?.Name?[0] == 'S';
        //    };

        //    decimal priceFilterTotal = productArray
        //        .Filter(FilterByPrice)
        //        .TotalPrices();
        //    decimal nameFilterTotal = productArray
        //        .Filter(nameFilter)
        //        .TotalPrices();

        //    return View("Index", new string[] { $"Array Total: {priceFilterTotal:C2}", $"Name Total: {nameFilterTotal:C2}" });
        //}

        //public ViewResult Index()
        //{
        //    Product[] productArray = {
        //        new Product { Name = "Kayak", Price = 275M},
        //        new Product { Name = "Lifejacket", Price = 48.95M },
        //        new Product { Name = "Soccer Ball", Price = 19.50M },
        //        new Product { Name = "Corner Flag", Price = 34.95M }
        //    };


        //    decimal priceFilterTotal = productArray
        //        .Filter(p => (p?.Price ?? 0) >= 20)
        //        .TotalPrices();
        //    decimal nameFilterTotal = productArray
        //        .Filter(p => p?.Name?[0] == 'S')
        //        .TotalPrices();

        //    return View("Index", new string[] { $"Array Total: {priceFilterTotal:C2}", $"Name Total: {nameFilterTotal:C2}" });
        //}

        //public ViewResult Index() {
        //    return View(Product.GetProducts().Select(p => p?.Name));
        //}

        public ViewResult Index() => View(Product.GetProducts().Select(p => p?.Name));


    }

}