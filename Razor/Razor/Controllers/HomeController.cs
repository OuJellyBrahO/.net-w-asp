﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Razor.Models;

namespace Razor.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            //Product p = new Product()
            //{
            //    ProductId = 1,
            //    Name = "Kayak",
            //    Description = "Single person boat",
            //    Category = "Watersport",
            //    Price = 275M
            //};

            Product[] arr = new Product[]
            {
                new Product{Name = "Kayak", Price = 275M},
                new Product{Name = "Canoe", Price = 800M},
                new Product{Name = "Paddle", Price = 15M}
            };

            ViewBag.StockLevel = 2;

            return View(arr);
        }
    }
}