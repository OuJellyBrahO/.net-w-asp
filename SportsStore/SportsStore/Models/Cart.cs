﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();
        public IEnumerable<CartLine> Lines => lineCollection;

        public virtual void AddItem(Product prod, int qty)
        {
            CartLine line = lineCollection
                //.Where(l => l.Product.ProductId == prod.ProductId)
                //.FirstOrDefault();
                .FirstOrDefault(l => l.Product.ProductId == prod.ProductId);

            if (line == null)
            {
                //Does Not Exist...add to cart
                lineCollection.Add(
                    new CartLine()
                    {
                        Product = prod,
                        Quantity = qty
                    }
                    );
            }
            else
            {
                line.Quantity += qty;
            }
        }

        public virtual void Clear() => lineCollection.Clear();

        public virtual void RemoveLine(Product prod)
        {
            lineCollection.RemoveAll(l => l.Product.ProductId == prod.ProductId);
        }

        public virtual decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Product.Price * e.Quantity);
        }



        
    }

    public class CartLine
    {
        public int CartLineId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
