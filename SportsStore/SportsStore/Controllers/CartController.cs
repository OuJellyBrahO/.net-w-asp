﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Infrastructure;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using Microsoft.AspNetCore.Http;

namespace SportsStore.Controllers
{
    public class CartController : Controller
    {
        private IProductRepository prodRepo;
        private Cart cart;

        public CartController(IProductRepository prodRepo, Cart cartService)
        {
            this.prodRepo = prodRepo;
            cart = cartService;
        }

        public ViewResult Index(string returnURL)
        {
            return View(
                new CartIndexViewModel
                {
                    Cart = cart,
                    ReturnURL = returnURL
                }
                );
        }



        public RedirectToActionResult AddToCart(int productId, string returnURL)
        {
            Product prod = prodRepo.Products.FirstOrDefault(p => p.ProductId == productId);

            if (prod != null)
            {
                cart.AddItem(prod, 1);
            }


            return RedirectToAction("Index", new { returnURL });
        }

        public RedirectToActionResult RemoveFromCart(int productId, string returnURL)
        {
            Product prod = prodRepo.Products.FirstOrDefault(p => p.ProductId == productId);

            if (prod != null)
            {
                cart.RemoveLine(prod);
            }


            return RedirectToAction("Index", new { returnURL });
        }
    }
}