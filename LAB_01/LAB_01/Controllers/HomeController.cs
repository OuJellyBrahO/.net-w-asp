﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB_01.Models;
using Microsoft.AspNetCore.Mvc;

namespace LAB_01.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View(Repository.Cds);
        }

        [HttpGet]
        public ViewResult AddCdForm()
        {
            return View();
        }

        [HttpPost]
        public ViewResult AddCdForm(AddCd cd)
        {
            if (ModelState.IsValid)
            {
                Repository.AddCd(cd);
                return View("Index", Repository.Cds);
            }
            else
            {
                return View(cd);
            }
        }
    }
}