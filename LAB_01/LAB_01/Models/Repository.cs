﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_01.Models
{
    public class Repository
    {
        private static List<AddCd> cds = new List<AddCd>();

        public static IEnumerable<AddCd> Cds {
            get
            {
                return cds;
            }
        }

        public static void AddCd(AddCd cd)
        {
            cds.Add(cd);
        }
    }
}
