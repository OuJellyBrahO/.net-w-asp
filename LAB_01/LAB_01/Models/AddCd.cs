﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_01.Models
{
    public class AddCd
    {
        [Required(ErrorMessage = "Please Select A Bank")]
        public string Bank { get; set; }

        [Required(ErrorMessage = "Please Select A Term")]
        public int? Term { get; set; }

        [Required(ErrorMessage = "Please Enter A Rate")]
        [DataType(DataType.Currency)]
        public decimal? Rate { get; set; }

        [Required(ErrorMessage = "Please Enter An Amount")]
        [DataType(DataType.Currency)]
        public decimal? DepositAmount { get; set; }

        public decimal? MaturityAmount {
            get {
                // A = P(1 + r/n)^nt
                double P = Convert.ToDouble(this.DepositAmount.Value);
                double r = Convert.ToDouble(this.Rate.Value) / 100;
                double t = this.Term.Value / 12;
                decimal A = Convert.ToDecimal(P * Math.Pow((1 + (r / 365)),365 * t));

                return A;
            }
        }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter A Date")]
        public DateTime? PurchaseDate { get; set; }

        public DateTime? MaturityDate {
            get {
                return this.PurchaseDate.Value.AddMonths(this.Term.Value);
            }
        }

    }
}
