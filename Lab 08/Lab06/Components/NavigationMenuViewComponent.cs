﻿using Lab06.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private IDepartmentRepository repository;

        public NavigationMenuViewComponent(IDepartmentRepository repo)
        {
            repository = repo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedDepartment = RouteData?.Values["department"];
            ViewBag.TEST = "TESTING";
            return View(repository.Departments
                .Select(x => x.DepartmentName)
                .Distinct()
                .OrderBy(x => x));
        }
    }
}
