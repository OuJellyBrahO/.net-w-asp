﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class SubmissionController : Controller
    {
        private ISubmissionRepository repository;
        private Review review;

        public SubmissionController(ISubmissionRepository repoService, Review reviewService)
        {
            repository = repoService;
            review = reviewService;
        }

        public ViewResult List() =>
            View(repository.Submissions.Where(s => !s.Completed));

        [HttpPost]
        public IActionResult MarkCompleted(int submissionId)
        {
            Submission submission = repository.Submissions
                .FirstOrDefault(s => s.SubmissionID == submissionId);
            if (submission != null)
            {
                submission.Completed = true;
                repository.SaveSubmission(submission);
            }
            return RedirectToAction(nameof(List));
        }


        public ViewResult Submit() => View(new Submission());

        [HttpPost]
        public IActionResult Submit(Submission submission)
        {
            if (review.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, there are no employees selected for review!");
            }
            if (ModelState.IsValid)
            {
                submission.Lines = review.Lines.ToArray();
                repository.SaveSubmission(submission);
                return RedirectToAction(nameof(Completed));
            }
            else
            {
                return View(submission);
            }
        }

        public ViewResult Completed()
        {
            review.Clear();
            return View();
        }
    }
}