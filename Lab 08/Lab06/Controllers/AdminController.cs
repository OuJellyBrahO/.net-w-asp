﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class AdminController : Controller
    {
        private IEmployeeRepository repository;

        public AdminController(IEmployeeRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index() => View(repository.Employees);

        public ViewResult Edit(int employeeId) =>
            View(repository.Employees
                .FirstOrDefault(e => e.EmployeeId == employeeId));

        [HttpPost]
        public IActionResult Edit(Employee employee)
        {
            if (ModelState.IsValid)
            {
                repository.SaveEmployee(employee);
                TempData["message"] = $"{employee.FirstName} {employee.LastName} has been edited succesfully.";
                return RedirectToAction("Index");
            }
            else
            {
                return View(employee);
            }
        }

        public ViewResult Create() => View("Edit", new Employee());

        [HttpPost]
        public IActionResult Delete(int employeeID)
        {
            Employee deletedEmployee = repository.DeleteEmployee(employeeID);
            if (deletedEmployee != null)
            {
                TempData["message"] = $"{deletedEmployee.FirstName} {deletedEmployee.LastName} was terminated.";
            }
            return RedirectToAction("Index");
        }
    }
}