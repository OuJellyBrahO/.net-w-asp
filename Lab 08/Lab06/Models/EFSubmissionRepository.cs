﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFSubmissionRepository : ISubmissionRepository
    {
        private StoreDbContext context;

        public EFSubmissionRepository(StoreDbContext ctx)
        {
            context = ctx;
        }
        public IEnumerable<Submission> Submissions => context.Submissions
            .Include(s => s.Lines)
            .ThenInclude(l => l.Employee);

        public void SaveSubmission(Submission submission)
        {
            context.AttachRange(submission.Lines.Select(l => l.Employee));
            if (submission.SubmissionID == 0)
            {
                context.Submissions.Add(submission);
            }
            context.SaveChanges();
        }
    }
}
