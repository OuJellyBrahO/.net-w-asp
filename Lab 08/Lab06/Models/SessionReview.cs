﻿using Lab06.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class SessionReview : Review
    {
        public static Review GetReview(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;

            SessionReview review = session?.GetJson<SessionReview>("Review")
                ?? new SessionReview();
            review.Session = session;
            return review;

            
        }

        [JsonIgnore]
        public ISession Session { get; set; }

        public override void AddEmployee(Employee employee)
        {
            base.AddEmployee(employee);
            Session.SetJson("Review", this);
        }

        public override void RemoveEmployee(Employee employee)
        {
            base.RemoveEmployee(employee);
            Session.SetJson("Review", this);
        }

        public override void Clear()
        {
            base.Clear();
            Session.Remove("Review");
        }
    }
}
