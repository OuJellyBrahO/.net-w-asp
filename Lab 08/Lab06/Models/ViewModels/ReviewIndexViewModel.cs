﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models.ViewModels
{
    public class ReviewIndexViewModel
    {
        public Review Review {get; set; }
        public string ReturnUrl { get; set; }
    }
}
