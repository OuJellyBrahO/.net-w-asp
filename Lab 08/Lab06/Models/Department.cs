﻿using System.Collections.Generic;

namespace Lab06.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        //Associations
        public List<DepartmentEmployee> DepartmentEmployees { get; set; }
        public List<DepartmentManager> DepartmentManagers { get; set; }

    }
}