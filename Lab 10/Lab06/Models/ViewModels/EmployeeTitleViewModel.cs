﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models.ViewModels
{
    public class EmployeeTitleViewModel
    {
        public Employee Employee { get; set; }
        public IEnumerable<Title> Titles { get; set; }
        public IEnumerable<Department> Departments { get; set; }
        public DepartmentEmployee CurrentDepartment { get; set; }
    }
}
