﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models.ViewModels
{
    public class DepartmentListViewModel
    {
        public IEnumerable<Department> Departments { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentDepartment { get; set; }
    }
}
