﻿using System;

namespace Lab06.Models
{
    public class DepartmentManager
    {
        public int DepartmentManagerId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        //Associations
        public Department Department { get; set; }
        public int DepartmentId { get; set; }

        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }
    }
}