﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static Lab06.Models.Review;

namespace Lab06.Models
{
    public class Submission
    {
        [BindNever]
        public int SubmissionID { get; set; }
        [BindNever]
        public ICollection<ReviewLine> Lines { get; set; }
        [BindNever]
        public bool Completed { get; set; }

        [Required(ErrorMessage = "Please enter a name.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a deadline date.")]
        public DateTime DueDate { get; set; }

        [Required(ErrorMessage = "Please enter a comment.")]
        public string Comment { get; set; }

        public bool Urgent { get; set; }
    }
}
