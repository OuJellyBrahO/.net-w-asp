﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFDepartmentManagerRepository : IDepartmentManagerRepository
    {
        private StoreDbContext context;

        public EFDepartmentManagerRepository(StoreDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<DepartmentManager> DepartmentManagers => context.DepartmentManagers;
    }
}
