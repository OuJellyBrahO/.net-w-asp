﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFEmployeeRepository : IEmployeeRepository
    {
        private StoreDbContext context;
        public IEnumerable<Employee> Employees => context.Employees
            //.Include(de => de.DepartmentEmployees)
            .Include(t => t.Title)
            //.Include(dm => dm.DepartmentManagers)
            
            ;

        public EFEmployeeRepository(StoreDbContext context)
        {
            this.context = context;
        }

        public void SaveEmployee(Employee employee)
        {
            if (employee.EmployeeId == 0)
            {
                context.Employees.Add(employee);
            }
            else
            {
                Employee dbEntry = context.Employees
                    .FirstOrDefault(e => e.EmployeeId == employee.EmployeeId);
                if (dbEntry != null)
                {
                    dbEntry.FirstName = employee.FirstName;
                    dbEntry.LastName = employee.LastName;
                    dbEntry.Gender = employee.Gender;
                    dbEntry.HireDate = employee.HireDate;
                    dbEntry.TerminationDate = employee.TerminationDate;
                    dbEntry.TitleId = employee.TitleId;
                }
            }
            context.SaveChanges();
        }

        public Employee DeleteEmployee(int employeeID)
        {
            Employee dbEntry = context.Employees
                .FirstOrDefault(e => e.EmployeeId == employeeID);
            if (dbEntry != null)
            {
                dbEntry.TerminationDate = DateTime.Now;
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
