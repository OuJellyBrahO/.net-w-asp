﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Lab06.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private IEmployeeRepository repository;
        private ITitleRepository titleRepo;
        private IDepartmentRepository departmentRepo;
        private IDepartmentEmployeeRepository departmentEmployeeRepository;
        private IDepartmentManagerRepository departmentManagerRepository;

        public AdminController(IEmployeeRepository repo, ITitleRepository tRepo, IDepartmentRepository dRepo, IDepartmentEmployeeRepository deRepo, IDepartmentManagerRepository dmRepo)
        {
            repository = repo;
            titleRepo = tRepo;
            departmentRepo = dRepo;
            departmentEmployeeRepository = deRepo;
            departmentManagerRepository = dmRepo;
        }

        public ViewResult Index() => View(repository.Employees);

        //public ViewResult Edit(int employeeId) =>
        //    View(repository.Employees
        //        .FirstOrDefault(e => e.EmployeeId == employeeId));

        public IActionResult Edit(int employeeId)
        {
            if (departmentManagerRepository.DepartmentManagers.FirstOrDefault(dm => dm.EmployeeId == employeeId) == null)
            {
                return View(new EmployeeTitleViewModel
                {
                    Employee = repository.Employees
                        .FirstOrDefault(e => e.EmployeeId == employeeId),
                    Titles = titleRepo.Titles,
                    Departments = departmentRepo.Departments,
                    CurrentDepartment = departmentEmployeeRepository.DepartmentEmployees.FirstOrDefault(de => de.EmployeeId == employeeId)

                });
            }
            else
            {
                TempData["message"] = "Managers cannot be edited";
                return RedirectToAction("Index");
            }

        }
            

        [HttpPost]
        public IActionResult Edit(Employee employee, string deptId)
        {
            if (deptId == null)
            {
                ModelState.AddModelError("", "Invalid name or password.");
            }

            if (ModelState.IsValid)
            {
                repository.SaveEmployee(employee);
                departmentEmployeeRepository.SaveEntry(employee, deptId);
                TempData["message"] = $"{employee.FirstName} {employee.LastName} has been added succesfully.";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["message"] = "Please Select A Department!";
                return View(new EmployeeTitleViewModel
                {
                    Employee = employee,
                    Titles = titleRepo.Titles,
                    Departments = departmentRepo.Departments,
                    CurrentDepartment = departmentEmployeeRepository.DepartmentEmployees.FirstOrDefault(de => de.EmployeeId == employee.EmployeeId)

                });
            }
        }

        public ViewResult Create() => View("Edit", new EmployeeTitleViewModel
        {
            Employee = new Employee(),
            Titles = titleRepo.Titles,
            Departments = departmentRepo.Departments,
            CurrentDepartment = null

        });

        [HttpPost]
        public IActionResult Delete(int employeeID)
        {
            if (departmentManagerRepository.DepartmentManagers.FirstOrDefault(dm => dm.EmployeeId == employeeID) == null)
            {
                Employee deletedEmployee = repository.DeleteEmployee(employeeID);
                if (deletedEmployee != null)
                {
                    TempData["message"] = $"{deletedEmployee.FirstName} {deletedEmployee.LastName} was terminated.";
                }
                return RedirectToAction("Index");
            }
            else
            {
                TempData["message"] = "Managers cannot be terminated";
                return RedirectToAction("Index");
            }
            
            
        }

        public ActionResult FillTitles(int deptId)
        {
            var titles = titleRepo.Titles.Where(t => t.DepartmentId == deptId);
            return Json(titles);
        }
    }
}