﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Lab06.Migrations
{
    public partial class redesigndatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentEmployees_Departments_departmentId",
                table: "DepartmentEmployees");

            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentEmployees_Employees_employeeId",
                table: "DepartmentEmployees");

            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentManagers_Departments_departmentId",
                table: "DepartmentManagers");

            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentManagers_Employees_employeeId",
                table: "DepartmentManagers");

            migrationBuilder.DropForeignKey(
                name: "FK_Titles_Employees_employeeId",
                table: "Titles");

            migrationBuilder.DropIndex(
                name: "IX_Titles_employeeId",
                table: "Titles");

            migrationBuilder.DropColumn(
                name: "employeeId",
                table: "Titles");

            migrationBuilder.DropColumn(
                name: "fromDate",
                table: "Titles");

            migrationBuilder.DropColumn(
                name: "hourlyWage",
                table: "Titles");

            migrationBuilder.DropColumn(
                name: "toDate",
                table: "Titles");

            migrationBuilder.DropColumn(
                name: "fromDate",
                table: "DepartmentEmployees");

            migrationBuilder.DropColumn(
                name: "toDate",
                table: "DepartmentEmployees");

            migrationBuilder.RenameColumn(
                name: "titleId",
                table: "Titles",
                newName: "TitleId");

            migrationBuilder.RenameColumn(
                name: "title",
                table: "Titles",
                newName: "TitleName");

            migrationBuilder.RenameColumn(
                name: "lastName",
                table: "Employees",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "hireDate",
                table: "Employees",
                newName: "HireDate");

            migrationBuilder.RenameColumn(
                name: "gender",
                table: "Employees",
                newName: "Gender");

            migrationBuilder.RenameColumn(
                name: "firstName",
                table: "Employees",
                newName: "FirstName");

            migrationBuilder.RenameColumn(
                name: "employeeId",
                table: "Employees",
                newName: "EmployeeId");

            migrationBuilder.RenameColumn(
                name: "departmentName",
                table: "Departments",
                newName: "DepartmentName");

            migrationBuilder.RenameColumn(
                name: "departmentId",
                table: "Departments",
                newName: "DepartmentId");

            migrationBuilder.RenameColumn(
                name: "toDate",
                table: "DepartmentManagers",
                newName: "ToDate");

            migrationBuilder.RenameColumn(
                name: "fromDate",
                table: "DepartmentManagers",
                newName: "FromDate");

            migrationBuilder.RenameColumn(
                name: "employeeId",
                table: "DepartmentManagers",
                newName: "EmployeeId");

            migrationBuilder.RenameColumn(
                name: "departmentId",
                table: "DepartmentManagers",
                newName: "DepartmentId");

            migrationBuilder.RenameColumn(
                name: "departmentManagerId",
                table: "DepartmentManagers",
                newName: "DepartmentManagerId");

            migrationBuilder.RenameIndex(
                name: "IX_DepartmentManagers_employeeId",
                table: "DepartmentManagers",
                newName: "IX_DepartmentManagers_EmployeeId");

            migrationBuilder.RenameIndex(
                name: "IX_DepartmentManagers_departmentId",
                table: "DepartmentManagers",
                newName: "IX_DepartmentManagers_DepartmentId");

            migrationBuilder.RenameColumn(
                name: "employeeId",
                table: "DepartmentEmployees",
                newName: "EmployeeId");

            migrationBuilder.RenameColumn(
                name: "departmentId",
                table: "DepartmentEmployees",
                newName: "DepartmentId");

            migrationBuilder.RenameColumn(
                name: "departmentEmployeeId",
                table: "DepartmentEmployees",
                newName: "DepartmentEmployeeId");

            migrationBuilder.RenameIndex(
                name: "IX_DepartmentEmployees_employeeId",
                table: "DepartmentEmployees",
                newName: "IX_DepartmentEmployees_EmployeeId");

            migrationBuilder.RenameIndex(
                name: "IX_DepartmentEmployees_departmentId",
                table: "DepartmentEmployees",
                newName: "IX_DepartmentEmployees_DepartmentId");

            migrationBuilder.AddColumn<string>(
                name: "TitleDescription",
                table: "Titles",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TitleId",
                table: "Employees",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Employees_TitleId",
                table: "Employees",
                column: "TitleId");

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentEmployees_Departments_DepartmentId",
                table: "DepartmentEmployees",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "DepartmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentEmployees_Employees_EmployeeId",
                table: "DepartmentEmployees",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentManagers_Departments_DepartmentId",
                table: "DepartmentManagers",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "DepartmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentManagers_Employees_EmployeeId",
                table: "DepartmentManagers",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Titles_TitleId",
                table: "Employees",
                column: "TitleId",
                principalTable: "Titles",
                principalColumn: "TitleId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentEmployees_Departments_DepartmentId",
                table: "DepartmentEmployees");

            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentEmployees_Employees_EmployeeId",
                table: "DepartmentEmployees");

            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentManagers_Departments_DepartmentId",
                table: "DepartmentManagers");

            migrationBuilder.DropForeignKey(
                name: "FK_DepartmentManagers_Employees_EmployeeId",
                table: "DepartmentManagers");

            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Titles_TitleId",
                table: "Employees");

            migrationBuilder.DropIndex(
                name: "IX_Employees_TitleId",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "TitleDescription",
                table: "Titles");

            migrationBuilder.DropColumn(
                name: "TitleId",
                table: "Employees");

            migrationBuilder.RenameColumn(
                name: "TitleId",
                table: "Titles",
                newName: "titleId");

            migrationBuilder.RenameColumn(
                name: "TitleName",
                table: "Titles",
                newName: "title");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Employees",
                newName: "lastName");

            migrationBuilder.RenameColumn(
                name: "HireDate",
                table: "Employees",
                newName: "hireDate");

            migrationBuilder.RenameColumn(
                name: "Gender",
                table: "Employees",
                newName: "gender");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "Employees",
                newName: "firstName");

            migrationBuilder.RenameColumn(
                name: "EmployeeId",
                table: "Employees",
                newName: "employeeId");

            migrationBuilder.RenameColumn(
                name: "DepartmentName",
                table: "Departments",
                newName: "departmentName");

            migrationBuilder.RenameColumn(
                name: "DepartmentId",
                table: "Departments",
                newName: "departmentId");

            migrationBuilder.RenameColumn(
                name: "ToDate",
                table: "DepartmentManagers",
                newName: "toDate");

            migrationBuilder.RenameColumn(
                name: "FromDate",
                table: "DepartmentManagers",
                newName: "fromDate");

            migrationBuilder.RenameColumn(
                name: "EmployeeId",
                table: "DepartmentManagers",
                newName: "employeeId");

            migrationBuilder.RenameColumn(
                name: "DepartmentId",
                table: "DepartmentManagers",
                newName: "departmentId");

            migrationBuilder.RenameColumn(
                name: "DepartmentManagerId",
                table: "DepartmentManagers",
                newName: "departmentManagerId");

            migrationBuilder.RenameIndex(
                name: "IX_DepartmentManagers_EmployeeId",
                table: "DepartmentManagers",
                newName: "IX_DepartmentManagers_employeeId");

            migrationBuilder.RenameIndex(
                name: "IX_DepartmentManagers_DepartmentId",
                table: "DepartmentManagers",
                newName: "IX_DepartmentManagers_departmentId");

            migrationBuilder.RenameColumn(
                name: "EmployeeId",
                table: "DepartmentEmployees",
                newName: "employeeId");

            migrationBuilder.RenameColumn(
                name: "DepartmentId",
                table: "DepartmentEmployees",
                newName: "departmentId");

            migrationBuilder.RenameColumn(
                name: "DepartmentEmployeeId",
                table: "DepartmentEmployees",
                newName: "departmentEmployeeId");

            migrationBuilder.RenameIndex(
                name: "IX_DepartmentEmployees_EmployeeId",
                table: "DepartmentEmployees",
                newName: "IX_DepartmentEmployees_employeeId");

            migrationBuilder.RenameIndex(
                name: "IX_DepartmentEmployees_DepartmentId",
                table: "DepartmentEmployees",
                newName: "IX_DepartmentEmployees_departmentId");

            migrationBuilder.AddColumn<int>(
                name: "employeeId",
                table: "Titles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "fromDate",
                table: "Titles",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "hourlyWage",
                table: "Titles",
                type: "Money",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "toDate",
                table: "Titles",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "fromDate",
                table: "DepartmentEmployees",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "toDate",
                table: "DepartmentEmployees",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Titles_employeeId",
                table: "Titles",
                column: "employeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentEmployees_Departments_departmentId",
                table: "DepartmentEmployees",
                column: "departmentId",
                principalTable: "Departments",
                principalColumn: "departmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentEmployees_Employees_employeeId",
                table: "DepartmentEmployees",
                column: "employeeId",
                principalTable: "Employees",
                principalColumn: "employeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentManagers_Departments_departmentId",
                table: "DepartmentManagers",
                column: "departmentId",
                principalTable: "Departments",
                principalColumn: "departmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DepartmentManagers_Employees_employeeId",
                table: "DepartmentManagers",
                column: "employeeId",
                principalTable: "Employees",
                principalColumn: "employeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Titles_Employees_employeeId",
                table: "Titles",
                column: "employeeId",
                principalTable: "Employees",
                principalColumn: "employeeId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
