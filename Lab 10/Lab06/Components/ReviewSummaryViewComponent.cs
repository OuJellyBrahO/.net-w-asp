﻿using Lab06.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Components
{
    public class ReviewSummaryViewComponent : ViewComponent
    {
        private Review review;

        public ReviewSummaryViewComponent(Review reviewService)
        {
            review = reviewService;
        }

        public IViewComponentResult Invoke()
        {
            return View(review);
        }
    }
}
