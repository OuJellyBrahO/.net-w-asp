﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Lab06
{
    public class Startup
    {
        IConfigurationRoot Configuration;
        public Startup(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .Build();
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddMemoryCache();
            services.AddSession();

            
            services.AddDbContext<StoreDbContext>(options =>
                options.UseSqlServer(
                    Configuration["Data:AcmeDepartmentEmployees:ConnectionString"]));

            services.AddDbContext<AppIdentityDbContext>(options =>
                options.UseSqlServer(
                    Configuration["Data:AcmeDepartmentIdentity:ConnectionString"]));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AppIdentityDbContext>();

            services.AddTransient<IEmployeeRepository, EFEmployeeRepository>();
            services.AddTransient<IDepartmentRepository, EFDepartmentRepository>();
            services.AddTransient<ITitleRepository, EFTitleRepository>();
            services.AddTransient<IDepartmentEmployeeRepository, EFDepartmentEmployeeRepository>();
            services.AddTransient<IDepartmentManagerRepository, EFDepartmentManagerRepository>();
            services.AddScoped<Review>(sp => SessionReview.GetReview(sp));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ISubmissionRepository, EFSubmissionRepository>();
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDefaultFiles();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }
            
            app.UseStaticFiles();
            app.UseSession();
            app.UseIdentity();

            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                        name: "Error",
                        template: "Error",
                        defaults: new { controller = "Error", action = "Error" }
                        );

                    routes.MapRoute(
                        name: null,
                        template: "{department}/Page{pageNo:int}",
                        defaults: new { controller = "Department", action = "List" }
                        );

                    routes.MapRoute(
                        name: null,
                        template: "Page{pageNo:int}",
                        defaults: new { controller = "Department", action = "List", pageNo = 1}
                        );

                    

                    routes.MapRoute(
                        name: null,
                        template: "{department}",
                        defaults: new { controller = "Department", action = "List", pageNo = 1 }
                        );

                    routes.MapRoute(
                        name: null,
                        template: "",
                        defaults: new { controller = "Department", action = "List" }
                        );

                    routes.MapRoute(name: null, template: "{controller}/{action}/{id?}");
                }
                );
        }
    }
}
