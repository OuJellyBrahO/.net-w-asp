﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Lab06.Migrations
{
    public partial class deptIdTitles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "test",
                table: "Titles",
                newName: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Titles_DepartmentId",
                table: "Titles",
                column: "DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Titles_Departments_DepartmentId",
                table: "Titles",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "DepartmentId",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Titles_Departments_DepartmentId",
                table: "Titles");

            migrationBuilder.DropIndex(
                name: "IX_Titles_DepartmentId",
                table: "Titles");

            migrationBuilder.RenameColumn(
                name: "DepartmentId",
                table: "Titles",
                newName: "test");
        }
    }
}
