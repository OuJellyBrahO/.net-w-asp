﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Lab06.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class DepartmentController : Controller
    {
        private IDepartmentRepository deptRepo;
        private int PageSize = 3;

        public DepartmentController(IDepartmentRepository repo)
        {
            deptRepo = repo;
        }

        public ViewResult List(string department, int pageNo = 1)
        {
            DepartmentListViewModel model = new DepartmentListViewModel();
            IEnumerable<Department> pagedDepartments;

            pagedDepartments = deptRepo.Departments
                .Where(d => department == null || d.DepartmentName == department)
                .OrderBy(d => d.DepartmentName)
                .Skip((pageNo - 1) * PageSize)
                .Take(PageSize);

            model.Departments = pagedDepartments;

            PagingInfo pInfo = new PagingInfo()
            {
                CurrentPage = pageNo,
                ItemsPerPage = PageSize,
                TotalItems = department == null ?
                    deptRepo.Departments
                    .Count() :

                    deptRepo.Departments
                    .Where(p => p.DepartmentName == department)
                    .Count()

            };

            model.CurrentDepartment = department;

            model.PagingInfo = pInfo;
            return View(model);

        }
        

        
    }
}