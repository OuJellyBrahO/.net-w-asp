﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Models;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class EmployeeController : Controller
    {
        private IEmployeeRepository employeeRepo;

        public EmployeeController(IEmployeeRepository employeeRepo)
        {
            this.employeeRepo = employeeRepo;
        }

        public IActionResult List()
        {
            return View(employeeRepo.Employees
                .OrderBy(e => e.EmployeeId)
                );
        }
    }
}