﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab06.Infrastructure;
using Lab06.Models;
using Lab06.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Lab06.Controllers
{
    public class ReviewController : Controller
    {
        private IEmployeeRepository repository;
        private Review review;

        public ReviewController(IEmployeeRepository repo, Review reviewService)
        {
            repository = repo;
            review = reviewService;
        }

        public ViewResult Index(string returnUrl)
        {
            return View(new ReviewIndexViewModel
            {
                Review = review,
                ReturnUrl = returnUrl
            });
        }

        public RedirectToActionResult AddEmployee(int employeeId, string returnUrl)
        {
            Employee employee = repository.Employees
                .FirstOrDefault(e => e.EmployeeId == employeeId);

            

            if (employee != null)
            {
                review.AddEmployee(employee);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToActionResult RemoveEmployee(int employeeId, string returnUrl)
        {
            Employee employee = repository.Employees
                .FirstOrDefault(e => e.EmployeeId == employeeId);

            if (employee != null)
            {
                review.RemoveEmployee(employee);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        
    }
}