﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class Review
    {
        private List<ReviewLine> lineCollection = new List<ReviewLine>();
        public IEnumerable<ReviewLine> Lines => lineCollection;

        public virtual void AddEmployee(Employee employee)
        {
            ReviewLine line = lineCollection
                .Where(e => e.Employee.EmployeeId == employee.EmployeeId)
                .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new ReviewLine
                {
                    Employee = employee
                    
                });
            }
        }

        public virtual void RemoveEmployee(Employee employee) =>
            lineCollection.RemoveAll(l => l.Employee.EmployeeId == employee.EmployeeId);

        public virtual void Clear() => lineCollection.Clear();

        

        
    }
    public class ReviewLine
    {
        public int ReviewLineID { get; set; }
        public Employee Employee { get; set; }
    }
}
