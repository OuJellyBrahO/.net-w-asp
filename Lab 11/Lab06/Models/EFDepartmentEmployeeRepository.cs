﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFDepartmentEmployeeRepository : IDepartmentEmployeeRepository
    {
        private StoreDbContext context;
        private IEmployeeRepository repository;
        private Employee currentEmployee;

        public EFDepartmentEmployeeRepository(StoreDbContext context, IEmployeeRepository repo)
        {
            this.context = context;
            repository = repo;
        }

        public IEnumerable<DepartmentEmployee> DepartmentEmployees => context.DepartmentEmployees;

        public void SaveEntry(Employee employee, string deptId)
        {


            
            DepartmentEmployee dbEntry = context.DepartmentEmployees
                .FirstOrDefault(de => de.EmployeeId == employee.EmployeeId);

            if (dbEntry != null)
            {
                //Existing Employee
                dbEntry.EmployeeId = employee.EmployeeId;
                dbEntry.DepartmentId = Int32.Parse(deptId);
            }
            else
            {
                //New Employee Employee
                currentEmployee = repository.Employees.FirstOrDefault(e => (e.FirstName == employee.FirstName && e.LastName == employee.LastName && e.HireDate == employee.HireDate));
                context.DepartmentEmployees.Add(
                    new DepartmentEmployee
                    {
                        EmployeeId = currentEmployee.EmployeeId,
                        DepartmentId = Int32.Parse(deptId)
                    }
                    );
            }

            context.SaveChanges();
        }
    }
}
