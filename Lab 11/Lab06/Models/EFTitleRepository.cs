﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFTitleRepository : ITitleRepository
    {
        private StoreDbContext context;

        public EFTitleRepository(StoreDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Title> Titles => context.Titles;
    }
}
