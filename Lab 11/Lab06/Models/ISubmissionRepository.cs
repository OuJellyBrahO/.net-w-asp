﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public interface ISubmissionRepository
    {
        IEnumerable<Submission> Submissions { get; }
        void SaveSubmission(Submission submission);
    }
}
