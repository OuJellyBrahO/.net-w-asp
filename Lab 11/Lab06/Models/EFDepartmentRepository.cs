﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab06.Models
{
    public class EFDepartmentRepository : IDepartmentRepository
    {
        private StoreDbContext context;
        public IEnumerable<Department> Departments => context.Departments
            .Include(dm => dm.DepartmentManagers)
            .ThenInclude(dme => dme.Employee)
            .Include(de => de.DepartmentEmployees)
            .ThenInclude(e => e.Employee)
            .ThenInclude(t => t.Title)
            
            ;

        public EFDepartmentRepository(StoreDbContext context)
        {
            this.context = context;
        }
    }
}
