﻿using System;

namespace Lab06.Models
{
    public class DepartmentEmployee
    {
        public int DepartmentEmployeeId { get; set; }



        //Associations
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }

        public Department Department { get; set; }
        public int DepartmentId { get; set; }
    }
}